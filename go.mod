module git.openprivacy.ca/cwtch.im/server

go 1.23.1

require (
	cwtch.im/cwtch v0.29.0
	git.openprivacy.ca/cwtch.im/tapir v0.6.0
	git.openprivacy.ca/openprivacy/connectivity v1.11.0
	git.openprivacy.ca/openprivacy/log v1.0.3
	github.com/gtank/ristretto255 v0.1.3-0.20210930101514-6bb39798585c
	github.com/mattn/go-sqlite3 v1.14.24
	golang.org/x/crypto v0.28.0
)

require (
	filippo.io/edwards25519 v1.0.0 // indirect
	git.openprivacy.ca/openprivacy/bine v0.0.5 // indirect
	github.com/gtank/merlin v0.1.1 // indirect
	github.com/mimoo/StrobeGo v0.0.0-20220103164710-9a04d6ca976b // indirect
	go.etcd.io/bbolt v1.3.6 // indirect
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/sys v0.26.0 // indirect
)
